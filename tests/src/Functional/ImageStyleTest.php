<?php

declare(strict_types=1);

namespace Drupal\Tests\protected_download\Functional;

use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\Site\Settings;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\TestFileCreationTrait;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;

/**
 * Tests functions for generating paths and URLs to image styles.
 *
 * @group protected_download
 */
class ImageStyleTest extends BrowserTestBase {

  use TestFileCreationTrait {
    getTestFiles as drupalGetTestFiles;
  }

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['file', 'image', 'protected_download'];

  /**
   * {@inheritdoc}
   */
  protected $profile = 'testing';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The image style name.
   */
  protected string $styleName = 'style_foo';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    ImageStyle::create([
      'name' => $this->styleName,
      'label' => $this->randomString(),
    ])->save();

    $fileSystem = $this->container->get(FileSystemInterface::class);
    assert($fileSystem instanceof FileSystemInterface);

    $protectedDir = $this->privateFilesDirectory . $this->randomMachineName(8);
    $fileSystem->prepareDirectory($protectedDir, FileSystemInterface::CREATE_DIRECTORY);

    // Enable protected stream wrapper in parent site.
    new Settings([
      'protected_download_file_path_protected' => $protectedDir,
    ] + Settings::getAll());

    // Enable protected stream wrapper in child site.
    $settings['settings']['protected_download_file_path_protected'] = (object) [
      'value' => $protectedDir,
      'required' => TRUE,
    ];
    $this->writeSettings($settings);
    $this->rebuildContainer();

    // Explicitly call register() again on the stream wrapper manager.
    $streamWrapperManager = $this->container->get(StreamWrapperManagerInterface::class);
    assert($streamWrapperManager instanceof StreamWrapperManager);
    $streamWrapperManager->register();
  }

  /**
   * Tests failure mode when source image is missing.
   */
  public function testImageStyleUrlForMissingSourceImage(): void {
    $styleEntity = ImageStyle::load($this->styleName);
    $this->assertNotNull($styleEntity);

    $nonExistentUri = 'protected://foo.png';
    $generated_url = $styleEntity->buildUrl($nonExistentUri);
    $this->drupalGet($generated_url);
    $this->assertSession()->statusCodeEquals(404);

    // Check that requesting a image style of a nonexistent image does not
    // create any new directories in the file system.
    $directory = 'protected://styles/' . $this->styleName . '/protected/' . $this->randomMachineName();

    $fileUrlGenerator = $this->container->get(FileUrlGeneratorInterface::class);
    assert($fileUrlGenerator instanceof FileUrlGeneratorInterface);

    $nonExistingUrl = $fileUrlGenerator
      ->generate($directory . '/' . $this->randomMachineName())
      ->setAbsolute()
      ->toString();

    $this->drupalGet($nonExistingUrl);
    $this->assertFalse(file_exists($directory), 'New directory was not created in the filesystem when requesting an unauthorized image.');
  }

  /**
   * Tests functions for generating paths and URLs to image styles.
   */
  public function testImageStyleUrlAndPath(): void {
    $styleEntity = ImageStyle::load($this->styleName);
    $this->assertNotNull($styleEntity);

    $scheme = 'protected';

    $fileSystem = $this->container->get(FileSystemInterface::class);
    assert($fileSystem instanceof FileSystemInterface);

    $imageFactory = $this->container->get(ImageFactory::class);
    assert($imageFactory instanceof ImageFactory);

    // Create the directories for the styles.
    $directory = $scheme . '://styles/' . $this->styleName;
    $fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);

    // Create a working copy of the file.
    $testImages = $this->drupalGetTestFiles('image');
    $this->assertIsObject($testImages[0]);
    $this->assertObjectHasProperty('uri', $testImages[0]);
    $file = File::create([
      'uri' => $testImages[0]->uri,
    ]);
    $file->save();

    $fileUri = $file->getFileUri();
    $this->assertNotNull($fileUri);
    $originalUri = $fileSystem->copy($fileUri, $scheme . '://', FileExists::Rename);

    // Get the URL of a file that has not been generated and try to create it.
    $generatedUri = $styleEntity->buildUri($originalUri);
    $this->assertFalse(file_exists($generatedUri), 'Generated file does not exist.');
    $generatedUrl = $styleEntity->buildUrl($originalUri);

    // Fetch the URL that generates the file.
    $this->drupalGet($generatedUrl);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertTrue(file_exists($generatedUri), 'Generated file does exist after we accessed it.');
    $generatedContent = file_get_contents($generatedUri);
    $this->assertNotFalse($generatedContent);
    $this->assertSession()->responseContains($generatedContent);

    $generatedImage = $imageFactory->get($generatedUri);
    $this->assertEquals($generatedImage->getMimeType(), $this->getSession()->getResponseHeader('Content-Type'), 'Expected Content-Type was reported.');
    $this->assertEquals($generatedImage->getFileSize(), $this->getSession()->getResponseHeader('Content-Length'), 'Expected Content-Length was reported.');

    // Fetch the URL a second time and ensure that proper cache control headers
    // are present on subsequent request.
    $this->drupalGet($generatedUrl);
    $this->assertSession()->statusCodeEquals(200);
    $content_type = $this->getSession()->getResponseHeader('Content-Type');
    $this->assertEquals('image/png', $content_type, 'Content-Type header is image/png');
    $content_length = $this->getSession()->getResponseHeader('Content-Length');
    $this->assertEquals(filesize($generatedUri), $content_length, 'Content-Length header returns correct length');
    $last_modified = $this->getSession()->getResponseHeader('Last-Modified');
    $generated_mtime = filemtime($generatedUri);
    $this->assertNotFalse($generated_mtime);
    $this->assertEquals(gmdate(DATE_RFC7231, $generated_mtime), $last_modified, 'Last-Modified header returns correct date');
    $cache_control = $this->getSession()->getResponseHeader('Cache-Control');
    $this->assertNotNull($cache_control, 'Cache-Control header is present');
    $this->assertStringContainsString('public', $cache_control, 'Cache-Control header contains private directive');
    $this->assertStringContainsString('max-age=', $cache_control, 'Cache-Control header contains max-age directive');
    $etag = $this->getSession()->getResponseHeader('ETag');
    $this->assertNotEmpty($etag, 'ETag header is present');
    $expire = $this->getSession()->getResponseHeader('Expires');
    $this->assertNotEmpty($expire, 'Expires header is present');
  }

}
