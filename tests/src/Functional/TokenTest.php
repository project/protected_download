<?php

declare(strict_types=1);

namespace Drupal\Tests\protected_download\Functional;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\TestFileCreationTrait;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\file\FileRepositoryInterface;
use Drupal\protected_download\ExactTtlPolicy;

/**
 * Tests token integration.
 *
 * @group protected_download
 */
class TokenTest extends BrowserTestBase {

  use TestFileCreationTrait {
    getTestFiles as drupalGetTestFiles;
  }

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['file', 'protected_download'];

  /**
   * {@inheritdoc}
   */
  protected $profile = 'testing';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The URI for a generated private file.
   */
  protected string $privateFileUri;

  /**
   * A file in the private directory.
   */
  protected FileInterface $privateFile;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $fileSystem = \Drupal::service(FileSystemInterface::class);
    assert($fileSystem instanceof FileSystemInterface);

    $fileRepository = \Drupal::service(FileRepositoryInterface::class);
    assert($fileRepository instanceof FileRepositoryInterface);

    // Prepare a private file.
    $testFiles = $this->drupalGetTestFiles('text');
    $this->assertIsObject($testFiles[0]);
    $this->assertObjectHasProperty('uri', $testFiles[0]);
    $file = File::create([
      'uri' => $testFiles[0]->uri,
    ]);
    $file->save();

    $privateFileDir = 'private://' . $this->randomMachineName(8) . '/';
    $fileSystem->prepareDirectory($privateFileDir, FileSystemInterface::CREATE_DIRECTORY);
    $this->privateFileUri = $privateFileDir . $file->getFilename();
    $this->privateFile = $fileRepository->move($file, $this->privateFileUri);
  }

  /**
   * Tests token integration.
   */
  public function testTokenIntegration(): void {
    $tokenService = \Drupal::token();
    $now = \Drupal::time()->getRequestTime();

    $dateFormatter = \Drupal::service(DateFormatterInterface::class);
    assert($dateFormatter instanceof DateFormatterInterface);

    $text = 'Your file is accessible until [file:protected-download-expire] using the following link [file:protected-download-url]';
    $pattern = '#^Your file is accessible until (.*) using the following link (.*)$#';
    $expectedDate = $now + ExactTtlPolicy::DEFAULT_EXACT_TTL;
    $expectedBase = Url::fromUri('base:/protected-download/private')->setAbsolute()->toString();

    $result = $tokenService->replace($text, ['file' => $this->privateFile]);
    $status = preg_match($pattern, $result, $matches);
    $this->assertEquals(1, $status);
    $this->assertArrayHasKey(1, $matches);
    $this->assertSame($dateFormatter->format($expectedDate, 'medium'), $matches[1]);
    $this->assertArrayHasKey(2, $matches);
    $this->assertSame(0, strpos($matches[2], $expectedBase));

    $text = 'Your file is accessible until [file:protected-download-expire:long] using the following link [file:protected-download-url]';
    $result = $tokenService->replace($text, ['file' => $this->privateFile]);
    $status = preg_match($pattern, $result, $matches);
    $this->assertEquals(1, $status);
    $this->assertArrayHasKey(1, $matches);
    $this->assertSame($dateFormatter->format($expectedDate, 'long'), $matches[1]);
    $this->assertArrayHasKey(2, $matches);
    $this->assertSame(0, strpos($matches[2], $expectedBase));
  }

}
