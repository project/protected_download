<?php

/**
 * @file
 * Provides token support for protected downloads.
 */

declare(strict_types=1);

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\file\FileInterface;
use Drupal\protected_download\LinkGeneratorInterface;
use Drupal\protected_download\PolicyRepositoryInterface;

/**
 * Implements hook_token_info().
 *
 * @phpstan-return mixed[]
 */
function protected_download_token_info(): array {
  $file['protected-download-expire'] = [
    'name' => t('Protected download expiry date'),
    'description' => t('The date when a protected download generated now will expire.'),
  ];

  $file['protected-download-url'] = [
    'name' => t('Protected download URL'),
    'description' => t('The web-accessible URL for the file protected with an authentication code.'),
  ];

  return [
    'types' => [],
    'tokens' => [
      'file' => $file,
    ],
  ];
}

/**
 * Implements hook_tokens().
 *
 * @phpstan-param array<string, string> $tokens
 * @phpstan-param array<string, mixed> $data
 * @phpstan-param array{'langcode'?: string, 'callback'?: callable, 'clear'?: boolean} $options
 * @phpstan-return mixed[]
 */
function protected_download_tokens(string $type, array $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata): array {
  $token_service = \Drupal::token();

  $url_options = ['absolute' => TRUE];
  if (isset($options['langcode'])) {
    $url_options['language'] = \Drupal::languageManager()->getLanguage($options['langcode']);
    $langcode = $options['langcode'];
  }
  else {
    $langcode = LanguageInterface::LANGCODE_DEFAULT;
  }
  $replacements = [];
  if ($type === 'file' && $data['file'] instanceof FileInterface) {
    $now = \Drupal::time()->getRequestTime();

    $fileUri = $data['file']->getFileUri();
    if ($fileUri !== NULL) {
      $scheme = StreamWrapperManager::getScheme($fileUri);

      $policy_repository = \Drupal::service(PolicyRepositoryInterface::class);
      assert($policy_repository instanceof PolicyRepositoryInterface);

      // Work around typing bug in StreamWrapperManagerInterface::getScheme()
      assert($scheme !== TRUE);
      if ($scheme !== FALSE) {
        $policy = $policy_repository->getPolicy($scheme);
        $protected_download_expire = $policy->expire($now);

        foreach ($tokens as $name => $original) {
          switch ($name) {
            case 'protected-download-url':
              $link_generator = \Drupal::service(LinkGeneratorInterface::class);
              assert($link_generator instanceof LinkGeneratorInterface);
              $url = $link_generator->generate($fileUri)
                ->setOptions($url_options)
                ->toString();
              $replacements[$original] = $url;
              break;

            case 'protected-download-expire':
              $date_formatter = \Drupal::service(DateFormatterInterface::class);
              assert($date_formatter instanceof DateFormatterInterface);
              $replacements[$original] = $date_formatter->format($protected_download_expire, 'medium', '', NULL, $langcode);
              break;
          }
        }

        if ($date_tokens = $token_service->findWithPrefix($tokens, 'protected-download-expire')) {
          $replacements += $token_service->generate('date', $date_tokens, ['date' => $protected_download_expire], $options, $bubbleable_metadata);
        }
      }
    }
  }

  return $replacements;
}
