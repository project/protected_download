<?php

declare(strict_types=1);

namespace Drupal\protected_download;

/**
 * Protected download policy for uncacheable responses with exact time-to-live.
 */
class ExactTtlPolicy implements PolicyInterface {

  /**
   * Default exact ttl policy: 7 days.
   */
  const DEFAULT_EXACT_TTL = 604800;

  /**
   * Constructs a new exact TTL policy.
   *
   * @param int $ttl
   *   Number of seconds the generated link should be valid.
   */
  public function __construct(protected int $ttl) {
  }

  /**
   * {@inheritdoc}
   */
  public function isPrivate(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function expire(int $time): int {
    return $time + $this->ttl;
  }

}
