<?php

declare(strict_types=1);

namespace Drupal\protected_download;

use Drupal\Core\Url;

/**
 * Defines an interface for protected download link generators.
 */
interface LinkGeneratorInterface {

  /**
   * Generates a HMAC protected URL for the given file.
   *
   * @param string $uri
   *   The file uri, e.g., private://ticket-12345.pdf.
   * @param int $expire
   *   (Optional) The expiry date as a UNIX timestamp.
   *
   * @return \Drupal\Core\Url
   *   The generated link.
   */
  public function generate(string $uri, ?int $expire = NULL): Url;

}
