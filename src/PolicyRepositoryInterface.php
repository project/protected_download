<?php

declare(strict_types=1);

namespace Drupal\protected_download;

/**
 * An interface for protected download cache control policy implementations.
 */
interface PolicyRepositoryInterface {

  /**
   * Returns configuration for given scheme.
   *
   * @return \Drupal\protected_download\PolicyInterface
   *   The cache control policy suitable for the given scheme.
   */
  public function getPolicy(string $scheme): PolicyInterface;

}
