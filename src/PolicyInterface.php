<?php

declare(strict_types=1);

namespace Drupal\protected_download;

/**
 * Defines an interface for a protected download cache control policy.
 */
interface PolicyInterface {

  /**
   * Returns TRUE if Cache-Control should be set to 'private'.
   */
  public function isPrivate(): bool;

  /**
   * Given the current time, returns the expiry timestamp.
   */
  public function expire(int $time): int;

}
