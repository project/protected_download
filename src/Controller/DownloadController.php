<?php

declare(strict_types=1);

namespace Drupal\protected_download\Controller;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\image\Entity\ImageStyle;
use Drupal\protected_download\PolicyRepositoryInterface;
use Drupal\protected_download\SecurityKeyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Defines a controller to server protected downloads.
 */
class DownloadController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(SecurityKeyInterface::class),
      $container->get(PolicyRepositoryInterface::class),
      $container->get(TimeInterface::class),
      $container->get(HttpKernelInterface::class),
    );
  }

  /**
   * Constructs a new protected download controller.
   *
   * @param \Drupal\protected_download\SecurityKeyInterface $securityKey
   *   The download security key service.
   * @param \Drupal\protected_download\PolicyRepositoryInterface $policyRepository
   *   The protected download policy repository.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The request time.
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $httpKernel
   *   The http kernel.
   */
  public function __construct(
    protected SecurityKeyInterface $securityKey,
    protected PolicyRepositoryInterface $policyRepository,
    protected TimeInterface $time,
    protected HttpKernelInterface $httpKernel,
  ) {
  }

  /**
   * Verifies an incoming request and delivers a HMAC protected file.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param string $scheme
   *   The URI scheme.
   * @param string $expire
   *   A hex-encoded UNIX timestamp.
   * @param string $hmac
   *   The message authentication code.
   */
  public function deliver(
    Request $request,
    string $scheme,
    string $expire,
    string $hmac,
  ): Response {
    $response = NULL;

    // Construct the internal URI and attempt to deliver the file.
    $target = (string) $request->query->get('file');
    $uri = $scheme . '://' . $target;
    if ($this->securityKey->verify($uri, $expire, $hmac)) {

      $stat = @stat($uri);
      if (is_array($stat)) {
        $response = new BinaryFileResponse($uri, 200);

        // Use the same ETag format as Apache.
        $filemtime = $stat['mtime'];
        $filesize = $stat['size'];
        $etag = '"' . dechex($stat['ino']) . '-' . dechex($filesize) . '-' . dechex($filemtime) . '"';
        $response->setEtag($etag);

        // Adapt max-age to remaining time according to the expiry timestamp.
        $response->setMaxAge((int) hexdec($expire) - $this->time->getRequestTime());

        // Set response to public/private depending on settings for scheme.
        if ($this->policyRepository->getPolicy($scheme)->isPrivate()) {
          $response->setPrivate();
        }

        // Enforce http cache validation.
        // FIXME: Check whether this is really necessary.
        if ($response->isNotModified($request)) {
          $response->setNotModified();
        }
      }
      else {
        // If no file exists, attempt to forward to image style download
        // controller.
        [$prefix, $styleId, $scheme, $rest] = explode("/", $target, 4);
        if ($prefix === 'styles') {
          $styleEntity = ImageStyle::load($styleId);
          if ($styleEntity) {
            $query = ['file' => $rest];

            if (!$this->config('image.settings')->get('suppress_itok_output')) {
              $itok = $styleEntity->getPathToken($scheme . "://" . $rest);
              $query[IMAGE_DERIVATIVE_TOKEN] = $itok;
            }

            $response = $this->forward(
              $request,
              'Drupal\image\Controller\ImageStyleDownloadController::deliver',
              [
                'scheme' => $scheme,
                'required_derivative_scheme' => $scheme,
                'image_style' => $styleEntity,
              ],
              $query,
            );
          }
        }
      }
    }

    if (isset($response)) {
      return $response;
    }
    else {
      throw new NotFoundHttpException();
    }
  }

  /**
   * Forwards the request to another controller.
   *
   * Adapted from
   * Symfony\Bundle\FrameworkBundle\Controller\AbstractController::forward()
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request to forward.
   * @param string $controller
   *   The controller to forward the request to.
   * @param mixed[] $attributes
   *   Request attributes the controller expects.
   * @param string[] $query
   *   Url query key value pairs.
   */
  protected function forward(
    Request $request,
    string $controller,
    array $attributes = [],
    array $query = [],
  ): Response {
    $attributes['_controller'] = $controller;
    $subRequest = $request->duplicate($query, NULL, $attributes);
    $subRequest->setSession($request->getSession());

    return $this->httpKernel->handle($subRequest, HttpKernelInterface::SUB_REQUEST);
  }

}
