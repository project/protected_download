<?php

declare(strict_types=1);

namespace Drupal\protected_download;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\Core\Site\Settings;

/**
 * ServiceProvider class for protected downloads services.
 */
class ProtectedDownloadServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container): void {
    // Register the protected file stream wrapper if a file path has been set.
    if (Settings::get('protected_download_file_path_protected')) {
      $container->register('stream_wrapper.protected', 'Drupal\protected_download\StreamWrapper\ProtectedStream')
        ->addTag('stream_wrapper', ['scheme' => 'protected']);
    }
  }

}
