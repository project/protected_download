<?php

declare(strict_types=1);

namespace Drupal\protected_download;

/**
 * Implements the protected download cache control policy repository.
 */
class PolicyRepository implements PolicyRepositoryInterface {

  /**
   * Returns a policy for the given scheme.
   */
  public function getPolicy(string $scheme): PolicyInterface {
    // Defaults.
    if ($scheme === 'private') {
      return new ExactTtlPolicy(ExactTtlPolicy::DEFAULT_EXACT_TTL);
    }
    else {
      return new AlignedTtlPolicy(AlignedTtlPolicy::DEFAULT_ALIGNED_MIN_TTL, AlignedTtlPolicy::DEFAULT_ALIGNED_MAX_TTL);
    }

    // FIXME: Make this configurable.
  }

}
