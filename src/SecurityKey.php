<?php

declare(strict_types=1);

namespace Drupal\protected_download;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\PrivateKey;
use Drupal\Core\Site\Settings;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;

/**
 * Provides methods to generate and check the HMAC for protected downloads.
 */
class SecurityKey implements SecurityKeyInterface {

  /**
   * Constructs a download security key service.
   *
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The request time service.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $streamWrapperManager
   *   The stream wrapper manager service.
   * @param string $hmacKey
   *   The HMAC key. Defaults protected_download_private_key setting and
   *   falls back to Drupal private key + hash salt.
   */
  public function __construct(
    protected TimeInterface $time,
    protected StreamWrapperManagerInterface $streamWrapperManager,
    protected ?string $hmacKey = NULL,
  ) {
    if (!isset($this->hmacKey)) {
      $preferredKey = Settings::get('protected_download_private_key');
      if (is_string($preferredKey)) {
        $this->hmacKey = $preferredKey;
      }
      else {
        $fallbackKey = \Drupal::service(PrivateKey::class)->get() . Settings::getHashSalt();
        $this->hmacKey = $fallbackKey;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function verify(string $uri, string $expire, string $hmac) {
    $result = FALSE;

    if (strlen($uri) && strlen($expire) && strlen($hmac)) {
      $calculated_hmac = Crypt::hmacBase64($uri . $expire, $this->hmacKey);
      $scheme = $this->streamWrapperManager::getScheme($uri);
      // Work around typing bug in StreamWrapperManagerInterface::getScheme()
      assert($scheme !== TRUE);
      $result = $scheme !== FALSE &&
        hash_equals($calculated_hmac, $hmac) &&
        $this->time->getRequestTime() <= hexdec($expire) &&
        $this->streamWrapperManager->isValidScheme($scheme);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function generate(string $uri, string $expire) {
    return Crypt::hmacBase64($uri . $expire, $this->hmacKey);
  }

}
