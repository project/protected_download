<?php

declare(strict_types=1);

namespace Drupal\protected_download;

/**
 * Protected download policy for responses cacheable within a given period.
 *
 * Computes an expiry date which is aligned according to fixed time frames.
 */
class AlignedTtlPolicy implements PolicyInterface {

  /**
   * Default minimum ttl for protected download links with aligned expiry date.
   *
   * Defaults to one day.
   */
  const DEFAULT_ALIGNED_MIN_TTL = 86400;

  /**
   * Default maximum ttl for protected download links with aligned expiry date.
   *
   * Defaults to 7 plus 1 days.
   */
  const DEFAULT_ALIGNED_MAX_TTL = self::DEFAULT_ALIGNED_MIN_TTL + 604800;

  /**
   * Constructs a new aligned TTL policy.
   *
   * @param int $minTtl
   *   Minimum number of seconds the generated link should be valid.
   * @param int $maxTtl
   *   Maximum number of seconds the generated link should be valid.
   */
  public function __construct(
    protected int $minTtl,
    protected int $maxTtl,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function isPrivate(): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function expire(int $time): int {
    $deltaTtl = $this->maxTtl - $this->minTtl;
    return (int) ($this->minTtl + $deltaTtl * ceil($time / $deltaTtl));
  }

}
