<?php

declare(strict_types=1);

namespace Drupal\protected_download\StreamWrapper;

use Drupal\Core\Site\Settings;
use Drupal\Core\StreamWrapper\LocalStream;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\protected_download\LinkGeneratorInterface;

/**
 * Defines a Drupal protected (protected://) stream wrapper class.
 */
class ProtectedStream extends LocalStream {

  /**
   * {@inheritdoc}
   */
  public static function getType(): int {
    return StreamWrapperInterface::LOCAL_NORMAL;
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string|TranslatableMarkup {
    return t('Protected files');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string|TranslatableMarkup {
    return t('Protected local files secured using a HMAC.');
  }

  /**
   * {@inheritdoc}
   */
  public function getDirectoryPath(): string {
    $path = Settings::get('protected_download_file_path_protected');
    return is_string($path) ? $path : "";
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalUrl(): string {
    $linkGenerator = \Drupal::service(LinkGeneratorInterface::class);
    assert($linkGenerator instanceof LinkGeneratorInterface);
    return $linkGenerator->generate($this->uri)->setAbsolute()->toString();
  }

}
