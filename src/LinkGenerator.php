<?php

declare(strict_types=1);

namespace Drupal\protected_download;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\Core\Url;

/**
 * Implements a protected download link generator.
 */
class LinkGenerator implements LinkGeneratorInterface {

  /**
   * Constructs a new link generator.
   *
   * @param \Drupal\protected_download\SecurityKeyInterface $securityKey
   *   The protected download security key service.
   * @param \Drupal\protected_download\PolicyRepositoryInterface $policyRepository
   *   The protected download config repository.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The request time service.
   */
  public function __construct(
    protected SecurityKeyInterface $securityKey,
    protected PolicyRepositoryInterface $policyRepository,
    protected TimeInterface $time,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function generate(string $uri, ?int $expire = NULL): Url {
    $scheme = StreamWrapperManager::getScheme($uri);
    $target = StreamWrapperManager::getTarget($uri);

    if ($scheme === FALSE || $target === FALSE) {
      throw new \InvalidArgumentException('Invalid uri');
    }

    if (!isset($expire)) {
      // Work around typing bug in StreamWrapperManagerInterface::getScheme()
      assert($scheme !== TRUE);
      $policy = $this->policyRepository->getPolicy($scheme);
      $expire = $policy->expire($this->time->getRequestTime());
    }
    $expire_string = dechex($expire);

    $hmac = $this->securityKey->generate($uri, $expire_string);
    return Url::fromRoute('protected_download.link', [
      'scheme' => $scheme,
      'expire' => $expire_string,
      'hmac' => $hmac,
      'filepath' => $target,
    ], [
      'path_processing' => FALSE,
    ]);
  }

}
