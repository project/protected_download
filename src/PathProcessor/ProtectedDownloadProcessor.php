<?php

declare(strict_types=1);

namespace Drupal\protected_download\PathProcessor;

use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a path processor to rewrite protected download URLs.
 *
 * As the route system does not allow arbitrary amount of parameters convert
 * the file path to a query parameter on the request.
 */
class ProtectedDownloadProcessor implements InboundPathProcessorInterface {

  /**
   * The path prefix to use.
   */
  protected string $prefix = '/protected-download';

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    $result = $path;

    if (str_starts_with($path, "{$this->prefix}/") && !$request->query->has('file')) {
      $rest = substr($path, strlen("{$this->prefix}/"));
      [$scheme, $expire, $hmac, $file] = explode("/", $rest, 4);
      $request->query->set('file', $file);
      $result = implode('/', [$this->prefix, $scheme, $expire, $hmac]);
    }

    return $result;
  }

}
